# A500HDDLED

This is a simple adapter for the Amiga 500 which routes the IDE activity signal to the keyboard Floppy LED. Floppy activity will still be shown normally. 

The first version of this adapter was designed with classic storage devices in mind. Recently, a growing number of IDE-adapters operates at 3.3V levels 
which might lead to permanently lit LEDs with simple transistor circuits. I've added a level shifter on version 2 of the adapter for that reason.


## Pictures

![V2 with straight and angled pins](https://gitlab.com/HenrykRichter/a500hddled/raw/master/Pics/ideled_A500v2.jpg)

![V1 installed in A500](https://gitlab.com/HenrykRichter/a500hddled/raw/master/Pics/ideled_a500.jpg)

## Parts list
- 1 PNP Transistor SOT23 (MMBT3906, BC807 or similar)
- 2 Resistors 4.7k (0603)
- 1 Resistor 15k (0805, alternative: 10k is also suitable)
- 1 Capacitor 100nF (0805)
- 1 SN74LV1T125 (SC70, optional but recommended)
- 1 Pin Strip RM2.54 (0.1") 8 positions
- 1 Pin Strip RM2.54 (0.1") 1 position (angled)
- 1 Socket Strip RM2.54 (0.1") 8 positions

## History
- V2 (2024) adds a level shifter to improve compatibility with some recent IDE adapter (typically SD2IDE) solutions. 
- V1 (2019) could be populated with SMD or THT parts. The KiCAD files are still available in the "V1" subfolder in "PCB".

